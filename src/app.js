import React from 'react';
import config from 'visual-config-exposer';
import styled, { css } from 'styled-components';

import Profile from './components/Profile/profile';
import SocialLinks from './components/SocialLinks/socialLinks';
import Links from './components/Links/links';

import './app.css';

const Main = styled.main`
  position: absolute;
  min-height: 100vh;
  min-width: 100vw;
  top: 0;
  left: 0;
  background-attachment: fixed;
  background-size: cover;
  display: flex;
  align-items: center;
  flex-direction: column;

  ${(props) =>
    props.bg &&
    css`
      background-image: url(${props.bg});
    `}
`;

const App = () => {
  return (
    <Main bg={config.settings.bgImg}>
      <section className="section_profile">
        <Profile />
        <SocialLinks />
      </section>
      <section className="section_links">
        <Links />
      </section>
    </Main>
  );
};

export default App;
